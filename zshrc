# Below are suggested entries to add to your ~/.zshrc to configure shell history and the shell prompt
# They are recommended from the perspective of a professional pen tester who often needs to reconstruct
# mission activities for report writing.  Hack the planet.
#
# Thanks to T. McLane for trailblazing the prompt and history configurations.

################################################################
# Shell Prompt                                                 #
# 1. Jazzes up the appearance                                  #
# 2. Adds YYYY.MM.DD and a live ticking HH:MM:SS to the prompt #
################################################################
PROMPT=$'%F{%(#.red.blue)}┌──(%F{51}%D{20%y.%m.%f} %F{%(#.red.blue)}| %F{51}%D{%H:%M:%S}%F{%(#.red.blue})──%F{%(#.green.green)}[%b%F{%(#.green.green)}%(6~.%-1~/…/%4~.%5~)]%F{%(#.red.blue)}\n└─%B%(#.%F{red}#.%F{blue}$)%b%F{reset} '
TMOUT=1
TRAPALRM() {
	zle reset-prompt
}

#######################################################################################
# History configurations                                                              #
# 1. Will blend all open shell session commands into one ~/.zsh_history file.         #
# 2. Applies a YYYY-MM-DD HH:MM to each command to help session reconstruction        #
# 3. Immediately commits command to history without waiting for graceful session exit #
#######################################################################################
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=2000000
export HISTTIMEFORMAT="[%F %T] "
setopt hist_expire_dups_first # Delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # Ignore duplicated commands history list
setopt hist_ignore_space      # Ignore commands that start with space
setopt hist_verify            # Show command with history expansion to user before running it
setopt share_history          # Share command history data
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY
alias history="history 0"     # Force zsh to show the complete history

##################################
# Add ~/.local/bin to your $PATH #
##################################
PATH=$HOME/.local/bin/:$PATH; export PATH
