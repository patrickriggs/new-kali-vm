# Customizations for a Web Attack VM
*Suggestions lifted from TheCyberMentor's Practical Ethical Hacking course*

* AssetFinder - Find's subdomains and other assets.  Recommended replacement for sublist3r.
    * `go get -u github.com/tomnomnom/assetfinder # Installation using go`
    * `assetfinder tesla.com`

* Amass - OWASP Amass performs network mapping of attack surfaces and external asset discovery using open source information gathering and active reconnaissance techniques.  Verify this is correct as [the install process](https://github.com/OWASP/Amass/blob/master/doc/install.md#from-source) has changed from 2019 to 2022`
    * `go install -v github.com/OWASP/Amass/v3/...@master` 
