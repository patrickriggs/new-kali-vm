# New Kali VM Deployment Resources
*Author: Patrick Riggs aka tac0shell*

These artifacts are to help rapidly deploy new Kali VM customizations.  Less time setting up, more time shooting packets.
