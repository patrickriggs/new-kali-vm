# Convert VMware Download for ESXi
*By: Patrick Riggs (tac0shell), February 2022*

I like to put my Kali deployment on my ESXi host so I can work on it either from my office with the really big curved monitor and no children, or in my living room from my laptop in front of the television.  Since there is no "for ESXi" image in the [Kali Virtual Machine](https://www.kali.org/get-kali/#kali-virtual-machines) offerings, there are a few steps you have to take to prepare the VMware .vmx for use on ESXi.

1. In the Virtual Machine's settings inside ESXi, disconnect the hard drive
2. Log into the ESXi host over SSH (needs to be enabled, obviously)
3. Find the uploaded (and extracted from .7z) directory with the Kali VM files.  My ESXi datastore is an NFS mount from a Synology NAS, so my uploads went to `/vmfs/volumes/datastoreName/Kali-Linux-2021.4a/`
4. Locate and verify the existence and name of the .vmx file.  Run the following command to convert it for ESXi use: `vmkfstools -i ./Kali-Linux-YYYY.Q-vmware-amd64.vmdk ./Kali-Linux-YYYY.Q-vmware-amd64-esxi.vmdk`
5. You should now have a workable copy of the vmdk file.  Add the VM as usual to ESXi.
6. Once the VM has been registered, boot it up and log in with default credentials (kali/kali).  
7. You will probably not have network connectivity.  Remove the network adapter from the VM settings in ESXi.  Then re-add it.
8. You should now have a working baseline image of Kali.  Snapshot the VM.

# Steps to Break-In Kali
## Patch System
1. `sudo apt update -y && sudo apt upgrade -y`

## Configure `root` Account
1. Set root password: `sudo passwd`
2. Permit GNOME/KDE UI root login: `sudo apt -y install kali-root-login`
3. Log out of kali desktop and login as root

## Configure Non-Privileged Account (`kali`)
1. Change default password: `passwd kali`
2. Change username to custom setting.  I am tac0shell: `usermod -l tac0shell kali`
    1. If you get the error `usermod: user kali is currently used by process ###`, make sure you actually LOGGED OUT of the kali desktop session and run the following to kill lingering processes: `pkill -9 -u kali`
3. Rename the group kali to tac0shell: `groupmod -n tac0shell kali`
4. Create the home directory for tac0shell and move the contents over (paired use of -d -m flags): `usermod -d /home/tac0shell -m tac0shell`
5. Verify the uid and gid, and the name and owners of the `/home/tac0shell` directory are correct: `ls -l /home && id tac0shell`

## Install Kali Metapackage
Metapackages are pre-packaged add-ons to Kali that are intended to help you rapidly deploy a specific niche instance of kali.  For instance, if you are going on a WiFi assessment, you can install all the WiFi penetration tools Kali has to offer by installing `kali-tools-802-11`.  If you are low on storage space, you can install `kali-tools-top10`
1. I install all the things: `apt install -y kali-linux-everything`
2. Log out of root desktop session and login as unprivileged user (tac0shell)

## Customize Your Shell Experience
These settings are geared towards being friendly to screenshots and reconstruction of your actions as an aid to report writing.

### Customize Default Kali *Terminal Emulator* Appearance
2. File > Preferences > Appearance
    1. Color scheme: BlackOnWhite (pentest report-centric) or BreezeModified (visually soothing)
    2. Application transparancy: 0%
3. File > Preferences > Behavior
    1. Unlimited history: Selected
    2. Confirm multiline paste: Checked
    3. Ask for confirmation when closing: Checked
    4. Open new terminals in current working directory: Checked

### Customize *Terminator* Appearance
1. Install Terminator: `sudo apt install terminator`
2. Open a Terminator session.  Right click on the in the main window and select Preferences.
3. In the Global tab, select the checkbox for "Re-use profiles for new terminals."
4. Click on the Profiles tab and create a new profile, *Professional*.  This will be a profile with settings friendly to report-writing.
5. With your new profile highlighted, click on the Scrolling tab and change the Scrollback setting to 9999999.
6. In the Background tab, make sure "Solid color" is selected.

## Customize zsh Experience
1. To configure your shell prompt, examine the suggested settings inside of [zshrc](https://gitlab.com/patrickriggs/new-kali/-/blob/main/zshrc) in the [new-kali-vm](https://gitlab.com/patrickriggs/new-kali-vm) GitLab repository and merge them into your non-privileged account's `~/.zshrc`.
2. Modify your zsh shell history to blend all open shells into one `.zsh_history` file, immediately commit them instead of waiting for the shell `exit` command, and timestamp each one to help reconstruct a timeline of past actions, use the suggested settings inside [zshrc](https://gitlab.com/patrickriggs/new-kali-vm/-/blob/main/zshrc)
3. Using the example settings inside the same file and add /home/tac0shell/.local/bin to your $PATH inside your ~/.zshrc.
## Transfer Files from Previous Kali
1. Install and enable ssh
    1. Install SSH: `sudo apt install ssh`
    2. Enable on reboot: `sudo systemctl enable ssh`
    3. Start service: `sudo systemctl start ssh`
2. Transfer `~/.mozilla`, `~/vpn`, and `~/.ssh` folders in whatever way preferable
    1. I use `tar` and `scp`
    2. Compress folder: `tar cvf folder.tar folder`
    3. Transfer: `scp folder.tar tac0shell@destinationIP:~`
    4. The .mozilla folder may already exist.  Move it to mozilla.bak and then decompress the mozilla.tar transfer to retain your bookmarks, extensions, cache, etc.
3. Clone personal gitlab repos or tarball your previous working folder(s) move to new VM (`~/hacking`, for me)

## Power and Screen Lock Settings
1. Right-click battery icon on top-right of xfce desktop
2. In the Power Manager window, click on "Display" tab and toggle the master switch on the top-right of that window
3. Slide the "Blank after" setting to your preferred setting (I use either Never or max it at 60 minutes), my work environment pending)

## Install and Configure Software
1. flameshot: `sudo apt install -y flameshot`
    1. Kali menu > Settings > Keyboard > Application Shortcuts > +Add
    2. Command: `/usr/bin/flameshot gui`
    3. Press desired shortcut/hotkey
2. pip and pip3: `sudo apt install python3-pip` and `sudo apt install python-pip`
5. TODO: [sumrecon stuff](https://github.com/Gr1mmie/sumrecon) (*assetfinder,amass,certspotter,httprobe,waybackurls,etc*)

## Shell Customization Options
## Set Up tmux Config
1. TODO

## Set System Time Zone
1. Right-click the time on the task bar, click Properties
2. Set your timezone and display preferences
3. Set timezone on command line
    1. `timedatectl list-timezones | grep *Berlin* or *New York*`
    2. `sudo timedatectl set-timezone *chosen timezone*`
